# Ref: https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/compute_keypair_v2
resource "openstack_compute_keypair_v2" "key" {
  name = var.cluster_name
}

resource "local_file" "private_key_ssh" {
  filename             = pathexpand("~/.ssh/${var.cluster_name}.pem")
  directory_permission = "0700"
  file_permission      = "0600"
  content              = openstack_compute_keypair_v2.key.private_key
}

resource "local_file" "private_key_init" {
  filename             = "${path.module}/${var.cluster_name}-init/${var.cluster_name}.pem"
  directory_permission = "0700"
  file_permission      = "0600"
  content              = openstack_compute_keypair_v2.key.private_key
}

resource "local_file" "ssh_config" {
  filename             = pathexpand("~/.ssh/config.d/${var.cluster_name}")
  directory_permission = "0700"
  file_permission      = "0600"
  content              = <<-EOT
# Automatically created by terraform

%{~ for x in openstack_compute_instance_v2.controlplane.* }
Host ${x.name}
  HostName ${x.network[1].fixed_ip_v4}
  StrictHostKeyChecking no
  UserKnownHostsFile=/dev/null
  IdentityFile ${pathexpand("~/.ssh/${var.cluster_name}.pem")}
  User centos

%{~ endfor }
%{~ for x in openstack_compute_instance_v2.worker.* }
Host ${x.name}
  HostName ${x.network[0].fixed_ip_v4}
  StrictHostKeyChecking no
  ProxyJump ${openstack_compute_instance_v2.controlplane[0].name}
  UserKnownHostsFile=/dev/null
  IdentityFile ${pathexpand("~/.ssh/${var.cluster_name}.pem")}
  User centos

%{~ endfor }
EOT
}
