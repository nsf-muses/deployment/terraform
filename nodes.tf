# boot image
data "openstack_images_image_v2" "boot" {
  name        = var.os
  most_recent = true
}

# openstack project name (bbXX)
data "openstack_identity_auth_scope_v3" "scope" {
  name = "my_scope"
}

# ----------------------------------------------------------------------
# control-plane nodes
# ----------------------------------------------------------------------

resource "openstack_compute_instance_v2" "controlplane" {
  count = var.controlplane_count
  depends_on = [
    openstack_networking_secgroup_rule_v2.same_security_group_ingress_tcp]
  name = "${var.cluster_name}-controlplane-${count.index}"
  image_name = var.os
  flavor_name = var.controlplane_flavor
  key_pair = openstack_compute_keypair_v2.key.name
  security_groups = [
    openstack_networking_secgroup_v2.cluster_security_group.name]
  config_drive = true

  user_data = <<-EOF
#!/bin/bash
echo "copy ssh key"
cat << EOF2 >/home/centos/.ssh/id_rsa
${openstack_compute_keypair_v2.key.private_key}
EOF2
chown centos /home/centos/.ssh/id_rsa
chmod 600 /home/centos/.ssh/id_rsa
echo "sleeping to wait for network"
while ! ping -c 1 -w 0  mirrorlist.centos.org > /dev/null ; do echo "Sleep 10s"; sleep 10; done
echo "mounting condo"
mkdir /condo
echo "radiant-nfs.ncsa.illinois.edu:/radiant/projects/${data.openstack_identity_auth_scope_v3.scope.project_name}/${var.cluster_name} /condo nfs defaults 0 0" >> /etc/fstab
mount -av
echo "install docker"
curl https://releases.rancher.com/install-docker/20.10.sh | sh
mkdir -p /etc/docker/
cat << EOF2 > /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF2
systemctl restart docker
systemctl enable docker
usermod -aG docker centos
echo "connect to rancher"
${rancher2_cluster.kube.cluster_registration_token.0.node_command} --address eth1 --internal-address eth0 --controlplane --etcd
echo "all done"
EOF

  block_device {
    uuid                  = data.openstack_images_image_v2.boot.id
    source_type           = "image"
    volume_size           = var.controlplane_disksize
    destination_type      = "volume"
    delete_on_termination = true
  }

  network {
    uuid = openstack_networking_network_v2.cluster_net.id
  }

  network {
    uuid = data.openstack_networking_network_v2.ext_net.id
  }

  lifecycle {
    ignore_changes = [
      key_pair,
      block_device,
      user_data
    ]
  }
}

# ----------------------------------------------------------------------
# worker nodes
# ----------------------------------------------------------------------

# this is needed it seems otherwise we can not add allowed_address_pairs
# which is needed to make metallb and loadbalancer work correctly. This
# does require us to manually delete all the IPs.
resource "openstack_networking_port_v2" "worker_ip" {
  count              = var.worker_count
  name               = "${var.cluster_name}-worker-${count.index}"
  network_id         = openstack_networking_network_v2.cluster_net.id
  security_group_ids = [openstack_networking_secgroup_v2.cluster_security_group.id]
  dynamic "allowed_address_pairs" {
    for_each = openstack_networking_port_v2.metallb_ip.*.all_fixed_ips.0
    content {
      ip_address = allowed_address_pairs.value
    }
  }
}

resource "openstack_compute_instance_v2" "worker" {
  count           = var.worker_count
  depends_on      = [openstack_networking_secgroup_rule_v2.same_security_group_ingress_tcp]
  name            = "${var.cluster_name}-worker-${count.index}"
  flavor_name     = var.worker_flavor
  key_pair        = openstack_compute_keypair_v2.key.name
  config_drive    = true
  security_groups = [ openstack_networking_secgroup_v2.cluster_security_group.name ]

  user_data       = <<-EOF
#!/bin/bash
echo "update hosts"
%{ for ip in openstack_networking_port_v2.worker_ip[count.index].all_fixed_ips }
echo "${ip} ${var.cluster_name}-worker-${count.index} $(hostname) $(hostname -f)"  >> /etc/hosts
%{ endfor }
echo "sleeping to wait for network"
while ! ping -c 1 -w 0  mirrorlist.centos.org > /dev/null ; do echo "Sleep 10s"; sleep 10; done
echo "install iscsi/nfs"
yum -y install iscsi-initiator-utils nfs-utils
echo "install docker"
curl https://releases.rancher.com/install-docker/20.10.sh | sh
mkdir -p /etc/docker/
cat << EOF2 > /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF2
systemctl restart docker
systemctl enable docker
usermod -aG docker centos
echo "connect to rancher"
${rancher2_cluster.kube.cluster_registration_token.0.node_command} --worker
echo "all done"
EOF

  block_device {
    uuid                  = data.openstack_images_image_v2.boot.id
    source_type           = "image"
    volume_size           = var.worker_disksize
    destination_type      = "volume"
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    port = element(openstack_networking_port_v2.worker_ip.*.id, count.index)
  }

  lifecycle {
    ignore_changes = [
      key_pair,
      block_device,
      user_data
    ]
  }
}
