data "openstack_networking_network_v2" "ext_net" {
  name = "ext-net"
}

resource "openstack_networking_network_v2" "cluster_net" {
  name           = "${var.cluster_name}-net"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "cluster_subnet" {
  name       = "${var.cluster_name}-subnet"
  network_id = openstack_networking_network_v2.cluster_net.id
  cidr       = "192.168.0.0/21"
  ip_version = 4
  dns_nameservers = ["141.142.2.2", "141.142.230.144"]
}

resource "openstack_networking_router_v2" "kube_router" {
  name                = "${var.cluster_name}-router"
  external_network_id = data.openstack_networking_network_v2.ext_net.id
  admin_state_up      = "true"
}

resource "openstack_networking_router_interface_v2" "kube_gateway" {
  router_id = openstack_networking_router_v2.kube_router.id
  subnet_id = openstack_networking_subnet_v2.cluster_subnet.id
}
